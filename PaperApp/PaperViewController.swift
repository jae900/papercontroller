//
//  ViewController.swift
//  PaperApp
//
//  Created by Jae Ki Lee on 19/08/2019.
//  Copyright © 2019 Jae Ki Lee. All rights reserved.
//

import UIKit

class PaperViewController: UIViewController {

    //MARK:- UseCaseInputPort
    let paperInputPort: PaperUseCaseInput = PaperUseCase()
    
    let paperTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Pleae type text here"
        return tf
    }()
    
    let paperLabel: UILabel = {
        let lb = UILabel()
        lb.isEnabled = false
        lb.backgroundColor = .black
        lb.textColor = .white
        return lb
    }()
    
    let paperButton: UIButton = {
        let bt = UIButton()
        bt.setTitle("Write text", for: .normal)
        bt.backgroundColor = .blue
        bt.addTarget(self, action: #selector(paperButtonTapped), for: .touchUpInside)
        return bt
    }()
    
    @objc func paperButtonTapped() {
        guard let letter = self.paperTextField.text else {return}
        paperInputPort.write(letter: letter, paperOutputPort: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let stakView = UIStackView(arrangedSubviews: [paperLabel, paperTextField, paperButton])
        stakView.axis = .vertical
        stakView.distribution = .fillEqually
        stakView.spacing = 20
        stakView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(stakView)
        stakView.centerXAnchor.constraint(equalToSystemSpacingAfter: self.view.centerXAnchor, multiplier: 0).isActive = true
        stakView.centerYAnchor.constraint(equalToSystemSpacingBelow: self.view.centerYAnchor, multiplier: 0).isActive = true
        
    }


}

//MARK:- UseCaseOutPut Influence
extension PaperViewController: PaperUseCaseOutput {
    
    func load(letter: String) {
        self.paperLabel.text = letter
    }
    
}
