//
//  Paper.swift
//  PaperApp
//
//  Created by Jae Ki Lee on 19/08/2019.
//  Copyright © 2019 Jae Ki Lee. All rights reserved.
//

class Paper {
    private var letter: String?
    
    init() {
        
    }
    
    init(letter: String) {
        self.letter = letter
    }
    
    func getLetter() -> String? {
        return letter
    }
    
    func write(letter: String) {
        self.letter = letter
    }
}
