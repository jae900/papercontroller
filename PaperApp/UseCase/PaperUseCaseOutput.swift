//
//  PaperUseCaseOutput.swift
//  PaperApp
//
//  Created by Jae Ki Lee on 19/08/2019.
//  Copyright © 2019 Jae Ki Lee. All rights reserved.
//

import Foundation

protocol PaperUseCaseOutput {
    func load(letter: String)
}
