//
//  PaperUseCase.swift
//  PaperApp
//
//  Created by Jae Ki Lee on 19/08/2019.
//  Copyright © 2019 Jae Ki Lee. All rights reserved.
//
import Foundation

class PaperUseCase: PaperUseCaseInput {
    
    let paper = Paper()
    
    init() {
        
    }
    
    func write(letter: String, paperOutputPort: PaperUseCaseOutput) {
        self.paper.write(letter: letter)
        guard let outputLetter = self.paper.getLetter() else {return}
        paperOutputPort.load(letter: outputLetter)
    }
    
}
